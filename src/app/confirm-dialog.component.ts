// import { MatDialogRef } from '@angular/material';
import { Component } from '@angular/core';

@Component({
    selector: 'confirm-dialog',
    template: `
        <div class="custom-confirm-box">
            <p><b>{{title}}</b></p>
            <p>{{message}}</p>
            <br/>
            <button type="button" class="btn green-gradient-btn" >Yes</button>
            <button type="button" class="btn yellow-gradient-btn" >No</button>
        </div>
    `,
})
export class ConfirmDialog {

    public title: string;
    public message: string;

    constructor(
        // public dialogRef: MatDialogRef<ConfirmDialog>
        ) {

    }
}