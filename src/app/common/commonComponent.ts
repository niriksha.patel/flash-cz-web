import { Component, OnInit } from '@angular/core';
import { Injector, NgZone,PLATFORM_ID } from '@angular/core';
import { CoolLocalStorage } from 'angular2-cool-storage';
import { ToasterService} from 'angular2-toaster';
import { Router } from "@angular/router";
import { CookieService } from 'ngx-cookie-service';
import { CommonService } from './common.service';
import { environment } from '../../environments/environment';
import { Http, Response,Headers,RequestOptions } from '@angular/http';
import { ErrorMessages } from './errorMessages';
import {Location} from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

import * as $ from 'jquery';
declare var jQuery:any;
declare var $ :any;

@Component({
  selector: 'parent-comp',
  template: `<h1>Hello from ParentComponent </h1>`,
  providers: []
})

export class BaseComponent {
  public URL=environment.apiUrl;
  constructor(injector: Injector) {    
        this.localStorage = injector.get(CoolLocalStorage) //Inject any service.
        this.toasterService = injector.get(ToasterService)
        this.router = injector.get(Router)
        this.cookie = injector.get(CookieService)
        this.commonService = injector.get(CommonService)
        this.http = injector.get(Http);
        this.errorMessages=injector.get( ErrorMessages);
        this.location=injector.get(Location);
        this.sanitize = injector.get(DomSanitizer);
        this.platformId = injector.get(PLATFORM_ID)
         // this.platformBrowser=injector.get(platformBrowser);
         // this.$=injector.get($);
        //console.log('Your current Environment is :', environment)
        //this.popToast('success','test','message')
      }

      public http = this.http;
      public router : Router;
      public $:any=$;
      public commonService : CommonService;
      public errorMessages : ErrorMessages;
      public location:Location;
      public  sanitize : DomSanitizer;
      public platformId : any;
     // public platformBrowser:any;
    //*************************************************************//
    //@Purpose : We can use following function to use localstorage (CoolLocalStorage).
    //*************************************************************//
    public localStorage : CoolLocalStorage;
    setToken(key,value){
      this.localStorage.setItem(key, value)
    }
    getToken(key){
      return this.localStorage.getItem(key)
    }
    removeToken(key){
      return this.localStorage.removeItem(key)
    }
    clearToken(){
      this.localStorage.clear()
    }
    //*************************************************************//


    //*************************************************************//
    //@Purpose : We can use following function to use cookie storage 
    //*************************************************************//
    public cookie : CookieService;
    setCookie(key,value){
      this.cookie.set(key, value)
    }
    getCookie(key){
      this.cookie.get(key)
    }
    removeCookie(key){
      this.cookie.delete(key)
    }
    clearCookie(){
      this.cookie.deleteAll()
    }
    //*************************************************************//

    //*************************************************************//
    //@Purpose : We can use following function to use Toaster Service.
    //*************************************************************//
    public toasterService : ToasterService;
    popToast(type,title,body){
      this.toasterService.pop(type,title,body)
    }

    //*************************************************************//
    //@Purpose : To prevent any key on keydown.
    //*************************************************************//
    keyDownHandler(event, key?) {
      if(key == 'space'){
        if (event.which === 32){
          event.preventDefault();
        }
      }
    }

    logout(){
      this.clearToken()
        //this.removeCookie('authToken')
        console.log('logout called ') //call logout api here.
        this.router.navigate(["/login"]);
      }

    /****************************************************************************
      @PURPOSE      : Test api call
      @PARAMETERS   : <user_info_obj>
      @RETURN       : <success_data>
      /****************************************************************************/
      callApi(){
           // let headers = new Headers({'content-Type':'application/json'});
           // let options = new RequestOptions({headers : headers});
           return this.http.get(environment.apiUrl+'posts/1')
           .map((response:Response) => response.json());
         }
         /****************************************************************************/

    //FORM VALIDATIONS
    getError(error,field){
      return this.errorMessages.getError(error,field);
    }
 //<!--Start of Tawk.to Script-->
 loadTawkScript(){
   var Tawk_API= Tawk_API||{}, Tawk_LoadStart=new Date();
   (function(){
     let s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
     s1.async=true;
     s1.src='https://embed.tawk.to/5aba49c6d7591465c708f4db/default';
     s1.charset='UTF-8';
     s1.setAttribute('crossorigin','*');
     s0.parentNode.insertBefore(s1,s0);
   })();
 }
// <!--End of Tawk.to Script-->

}