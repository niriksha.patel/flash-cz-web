import { Injectable, Injector } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, CanDeactivate } from '@angular/router';
import { CoolLocalStorage } from 'angular2-cool-storage';
import { Observable} from 'rxjs/Observable';

@Injectable()
export class CanLoginActivate implements CanActivate {

    constructor(public localStorage : CoolLocalStorage, public router: Router) {
    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
        //console.log('can login activate called')
        if(!this.localStorage.getItem("authToken")){
          return true;
        }
        this.router.navigate(['/main']);
        return false
    }
}


@Injectable()
export class AlwaysAuthChildrenGuard implements CanActivateChild {
  constructor(public localStorage : CoolLocalStorage, public router: Router) {
    }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    //console.log('can child activate called')
    if(this.localStorage.getItem("authToken")){
       return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}

 