import { BaseComponent } from './../common/commonComponent';
import { Injectable, Injector } from '@angular/core';
import { CoolLocalStorage } from 'angular2-cool-storage';
import { environment } from '../../environments/environment';
import { Http, Response,Headers,RequestOptions } from '@angular/http';
import { ToasterModule, ToasterService} from 'angular2-toaster';
import { Observable } from 'rxjs/Rx';
import { ConfirmDialog } from '../confirm-dialog.component';


@Injectable()
export class CommonService{
 
  authorised :any= false;
  URL=environment.apiUrl;
  public voucherCode:any;
  constructor(public localStorage:CoolLocalStorage,public http:Http,public toasterService: ToasterService ) { 
    //super(inj);
     this.authorise();
  }
  
  authorise() {
    if(this.localStorage.getItem("authToken")){
      this.authorised = true;
    }
    else{
      this.authorised = false;
    }
    //console.log('common called after',this.authorised)
  }

  /****************************************************************************
  @PURPOSE      : To Show Confirm Box.
  @PARAMETERS   :(title, message)
  @RETURN       : dialogRef
  ****************************************************************************/
  // public confirm(title: string, message: string): Observable<boolean> {
  //     let dialogRef: MatDialogRef<ConfirmDialog>;
  //     dialogRef = this.dialog.open(ConfirmDialog);
  //     dialogRef.componentInstance.title = title;
  //     dialogRef.componentInstance.message = message;
  //     return dialogRef.afterClosed();
  // }
  /****************************************************************************/


  /***************************** Toaster Message ***********************************************/
  popToast(success, title, body) {
    this.toasterService.pop(success, title, body);
  }
  /****************************************************************************/


  
  /*******************************************API for Login Call *******************************************************/
  login(userData){
     let headers = new Headers({'content-Type':'application/json'});
    let options = new RequestOptions({headers:headers});
    return this.http.post(environment.apiUrl+'api/userLogin',JSON.stringify(userData),options)
            .map((response:Response) => response.json());
  }
  /****************************************************************************************************************************/


  /*******************************************API for Login Call *******************************************************/
getAllCountries(){
   return this.http.get('https://restcountries.eu/rest/v2/all')
            .map((response:Response) => response.json());   
}
  /****************************************************************************************************************************/

/*******************************************API for getMsisdn *******************************************************/
getMsisdn(msisdn,countryCode){
   let headers = new Headers({'content-Type':'application/json'});
    let options = new RequestOptions({headers:headers});
    console.log("msisdn=",typeof(msisdn));
   return this.http.get(this.URL+'parseMSISDNForCountry/'+msisdn+'/'+countryCode+'/abcdefghioewrhfds12342345234234')
            .map((response:Response) => response.json());   
}
  /****************************************************************************************************************************/

/*******************************************API for operator amounts *******************************************************/
getProducts(providerId,countryCode){
   let headers = new Headers({'content-Type':'application/json'});
    let options = new RequestOptions({headers:headers});
   return this.http.get(this.URL+'getProducts/'+providerId+'/'+countryCode+'/abcdefghioewrhfds12342345234234')
            .map((response:Response) => response.json());   
}
  /****************************************************************************************************************************/

/*******************************************API for operator amounts *******************************************************/
purchaseProducts(data){
   let headers = new Headers({'content-Type':'application/json'});
    let options = new RequestOptions({headers:headers});
   return this.http.put(this.URL+'purchaseProducts/abcdefghioewrhfds12342345234234',JSON.stringify(data),options)
            .map((response:Response) => response.json());   
}
  /****************************************************************************************************************************/

/*******************************************API for Verify Vocuher *******************************************************/
verifyVoucher(voucherCode){
  this.voucherCode=voucherCode;
   let headers = new Headers({'content-Type':'application/json'});
    let options = new RequestOptions({headers:headers});
    let data={"transaction":{"voucherCode":voucherCode}}
   return this.http.post(this.URL+'verifyVoucher/abcdefghioewrhfds12342345234234',JSON.stringify(data),options)
            .map((response:Response) => response.json());   
}
  /****************************************************************************************************************************/

/*******************************************API for Redeem Vocuher *******************************************************/
redeemVoucher(msisdnData,voucherCode){
   let headers = new Headers({'Content-Type':'application/json'});
    let options = new RequestOptions({headers:headers});
    let data={"transaction": 
    {
      "voucherCode":voucherCode,
      "msisdn":msisdnData
    }
  }
   return this.http.post(this.URL+'redeemVoucher/abcdefghioewrhfds12342345234234',JSON.stringify(data),options)
            .map((response:Response) => response.json());   
}
  /****************************************************************************************************************************/

/*******************************************API for Checkout *******************************************************/
checkout(txId,data){
   let headers = new Headers({'Content-Type':'application/json'});
    let options = new RequestOptions({headers:headers});
    // let data={"request":this.voucherCode}
   return this.http.post(this.URL+'checkout/abcdefghioewrhfds12342345234234/'+txId,JSON.stringify(data),options)
            .map((response:Response) => response.json());   
}
  /****************************************************************************************************************************/


}