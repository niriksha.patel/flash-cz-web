import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import * as messages from './errorMessages.json';


  @Injectable()
  export class ErrorMessages {
  	public MSG = (<any>messages)

 constructor(public http: Http) {
  }
  		getError(error,field){
  			var message;
            if (error) {
                // console.log("error",error,fieldname);
                if (error.required) {
                    var required= this.MSG.ERROR.REQUIRED;
                    if (field == "msisdn") {
                        message =required.Msisdn;
                    } else if (field == "voucher") {
                        message =required.Voucher;
                    } else if (field == "surname") {
                        message =required.Surname;
                    } else if (field == "mobile") {
                        message =required.Mobile;
                    } else if (field == "password") {
                        message =required.Password;
                    } else if (field == "username") {
                        message =required.Username;
                    }else if (field == "secret") {
                        message =required.Secret;
                    } else if (field == "confirmPassword") {
                        message =required.ConfirmPassword;
                    } else if (field == "oldPassword") {
                        message =required.OldPassword;
                    } else if (field == "newPassword") {
                        message =required.NewPassword;
                    } else if (field == "lastname") {
                        message =required.Surname;
                    } 
                } else if (error.pattern) {
                      var pattern= this.MSG.ERROR.PATTERN;
                    if (field == "phonenumber") {
                        message = pattern.PhoneNumber;
                    } else if (field == "age") {
                        message = pattern.Age;
                    } else if (field == "email") {
                        message = pattern.Email;
                    } else if (field == "password" || field == "oldPassword" || field == "newPassword") {
                        message = pattern.Password;
                    } else if (field == "username") {
                        message = pattern.Username;
                    }else if (field == "mobile") {
                        message = pattern.Mobile;
                    }
                } else if (error.maxlength) {
                     var maxlength= this.MSG.ERROR.MAXLENGTH;
                    if (field == "phonenumber") {
                        message = maxlength.PhoneNumber;
                    } else if (field == "password" || field == "confirmPassword") {
                        message = maxlength.Password;
                    } else if (field == "oldPassword") {
                        message = maxlength.Password;
                    } else if (field == "newPassword") {
                        message = maxlength.Password;
                    } else if (field == "fname") {
                        message =maxlength.FirstName;
                    } else if (field == "lname") {
                        message =maxlength.LastName;
                    } else if (field == "username") {
                        message =maxlength.Username;
                    }
                } else if (error.minlength) {
                     var minlength= this.MSG.ERROR.MINLENGTH;
                    if (field == "phonenumber") {
                        message = minlength.PhoneNumber;
                    } else if (field == "password") {
                        message = minlength.Password;
                    } else if (field == "oldPassword") {
                        message = minlength.Password;
                    } else if (field == "newPassword") {
                        message = minlength.Password;
                    } else if (field == "pinno") {
                        message = minlength.PinNo;
                    } else if (field == "fname") {
                        message = minlength.FirstName;
                    } else if (field == "lname") {
                        message = minlength.LastName;
                    } else if (field == "username") {
                        message =minlength.Username;
                    }
                } else if (error.email) {
                    message = this.MSG.ERROR.PATTERN.Email;
                } 
                return message;
            }
  	    }
  }