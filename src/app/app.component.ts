import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { ToasterConfig} from 'angular2-toaster';
// declare let ga: Function;
declare let fbq: Function;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(private _router: Router, private _meta: Meta, private _title: Title) {
    this.isLoggedIn = false
  }
  public isLoggedIn : boolean;
  public toasterconfig : ToasterConfig = 
  new ToasterConfig({
    showCloseButton: false, 
    tapToDismiss: true, 
    timeout: 3000,
    limit : 1
  });

  ngOnInit() {
    this._router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // ga('set', 'page', event.urlAfterRedirects);
        // ga('send', 'pageview');
        fbq('track', event.urlAfterRedirects);
        // fbq('track', "ViewContent", {
        //   content_name: event.urlAfterRedirects,
        // });

        switch (event.urlAfterRedirects) {
          case '/':
          this._title.setTitle('Home Page');
          this._meta.updateTag({ name: 'description', content: 'Home Page Description' });
          break;
          case '/about':
          this._title.setTitle('About Page');
          this._meta.updateTag({ name: 'description', content: 'About Page Description' });
          break;
        }
      }
    });
  }
}
