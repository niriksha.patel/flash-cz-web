import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import { trigger , state , style , animate , transition, keyframes } from '@angular/animations';
import { Injector, NgZone } from '@angular/core'; 
import { BaseComponent } from '../../common/commonComponent';
import { PageScrollConfig, PageScrollService, PageScrollInstance } from 'ng2-page-scroll';
import * as $ from 'jquery';
declare var jQuery:any;
declare var $ :any;
declare const fbq: any;
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html'
})
export class HomePageComponent extends BaseComponent implements OnInit {
   // @ViewChild('msisdn')msisdn:ElementRef;
   public countriesData:any;
   public selectedCountryValue:any=1;
   public flagImage:any;
   public countryCallingCode:any;
   public countryselect:boolean=false;
   public phoneId:any;
   public user:any={};

   constructor(injector: Injector) {
     super(injector);
     console.log("hello");
     // this.localStorage.clear();
     // fbq('track', 'Home View');
     PageScrollConfig.defaultScrollOffset = 200;
     PageScrollConfig.defaultDuration = 500;
     this.loadTawkScript();

   }

   ngOnInit() {
    //this.popToast('success','Welcome','Successfully logged in.')
    // this.loadCountries();
    // console.log("this.msisdn",this.msisdn);
    // this.phoneId=this.msisdn.nativeElement;
    

  }
  ngAfterViewInit(){
    setTimeout(()=>{
      // $('body').removeClass('terms');

      $('iframe:nth-child(2)').css('display','block')
    },100)
  }
  /*======================= Call for Countries list in dropdown box =========================*/
  loadCountries(){
    // this.presentLoading();
    this.commonService.getAllCountries().subscribe(success=>{
    	console.log("countries list=",success);
    	this.countriesData=success;        
    },error=>{
      this.commonService.popToast('error','Error',error.message);
    });
  }
  /*======================= Call for when country is selected  =========================*/
  getMsisdn(countryValue){
    console.log("countryValue",countryValue);
    if(countryValue){
      this.countryselect=true;
      this.flagImage=countryValue.flag;
      this.countryCallingCode=countryValue.callingCodes[0];
      this.phoneId.focus();
    }
    else{
      this.countryselect=false;
    }

  // document.querySelectorBy
  // this.commonService.getMsisdn().subscribe(success =>{
  //     console.log("success",success);
  // },error=>{
  //      this.commonService.popToast('error','Error',error.message);
  //   });
}

getMsisdnOperator(numberValue){
  console.log("numberValue==",this.countryCallingCode+''+numberValue);
  // this.commonService.getMsisdn(this.countryCallingCode+''+numberValue).subscribe(success =>{
  //     console.log("success",success);
  //     if(success.result.isValid)
  //     {
  //       this.router.navigate(['../main/order-page']);
  //     }
  //     else{
  //        this.commonService.popToast('error','Error',"Please make sure that you have the correct number format and country selected");
  //     }
  // },error=>{
  //      this.commonService.popToast('error','Error',error.message);
  //   });
}




allowNumbers(e){
  var input;
  if (e.metaKey || e.ctrlKey) {
    return false;
  }
  if (e.which === 32) {
    return false;
  }
         // if(e.which > 53)
         // {  
         //   console.log("got")
         //   return false;
         // }
         if (e.which === 0) {
           return true;
         }
         if (e.which < 33) {
           return true;
         }
         if (e.which === 43 || e.which === 45) {
           return true;
         }
         input = String.fromCharCode(e.which);
         console.log("input=",input);
         return !!/[\d\s]/.test(input);
       }





     }
