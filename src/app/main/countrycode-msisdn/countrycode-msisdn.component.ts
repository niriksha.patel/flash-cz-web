import { Component, OnInit,ViewChild,ElementRef,Input } from '@angular/core';
import { trigger , state , style , animate , transition, keyframes } from '@angular/animations';
import { Injector, NgZone } from '@angular/core'; 
import { BaseComponent } from '../../common/commonComponent';

@Component({
  selector: 'app-countrycode-msisdn',
  templateUrl: './countrycode-msisdn.component.html',
  styleUrls: ['./countrycode-msisdn.component.css']
})
export class CountrycodeMsisdnComponent extends BaseComponent implements OnInit {
	@ViewChild('msisdn')msisdn:ElementRef;
	@Input('type')type:any;
  public flagImage:any;
  public countriesData:any;
  public selectedCountryValue:any='';
  public countryCallingCode:any;
  public countryselect:boolean=false;
  // public redeemFlag:boolean=false;
  public redeemFlag:boolean=true;
  public redeemData:boolean=false;
  public phoneId:any;
  public countryCode:any;
  public user:any={};
  public mask = [/\d/,/\d/,/\d/,/\d/,'-',/\d/,/\d/,/\d/,/\d/, '-', /\d/,/\d/,/\d/,/\d/,'-',/\d/,/\d/,/\d/,/\d/];
  constructor(injector: Injector) {
    super(injector);   
  }

  ngOnInit() {
    this.loadCountries();
  }

  ngAfterViewInit(){
    this.phoneId=this.msisdn.nativeElement; 
    console.log("this.phoenId",this.phoneId);
  }

  /*======================= Call for Countries list in dropdown box =========================*/
  loadCountries(){
    this.commonService.getAllCountries().subscribe(success=>{
    	this.countriesData=success; 
      let aa; 
      if(this.localStorage.getItem('msisdnData'))
      {	    	
        if(this.localStorage.getItem('msisdnCountry')){
          aa=JSON.parse(this.localStorage.getItem('msisdnCountry'));
        }
        this.user.selectedCountryValue='';
        if(this.type=="order")
        {
          this.user.selectedCountryValue=this.countriesData.find(o=>o.alpha2Code==aa.alpha2Code);
          this.user.msisdn= this.localStorage.getItem('msisdnData');          
        }
        this.getMsisdn(this.user.selectedCountryValue);
      }  
    },error=>{
      this.commonService.popToast('error','Error',error.message);
    });
  }
  /*======================= Call for when country is selected  =========================*/
  getMsisdn(countryValue){
    this.localStorage.setItem('msisdnCountry',JSON.stringify(countryValue));
    if(countryValue){
      this.countryselect=true;
      this.flagImage=countryValue.flag;
      this.countryCallingCode=countryValue.callingCodes[0];
      this.countryCode=countryValue.alpha2Code;
      console.log("this.phoneId",this.phoneId);
      setTimeout(()=>{
        this.phoneId.focus();
      },0);  
      console.log("after this.phoneId",this.phoneId);
    }
    else{
      this.countryselect=false;
    }
  }

  getMsisdnOperator(numberValue){
    this.localStorage.setItem('msisdnData',numberValue);
    this.commonService.getMsisdn(this.countryCallingCode+''+numberValue, this.countryCode).subscribe(success =>{
      console.log("success",success);
      if(success.result.isValid)
      {
        this.localStorage.setItem("operatorData",JSON.stringify(success.result.country.productProviders));
        this.router.navigate(['../cz/myflash/order-page']);
      }
      else{
        this.commonService.popToast('error','Error',"Please make sure that you have the correct number format and country selected");
      }
    },error=>{
      this.commonService.popToast('error','Error',error.message);
    });
  }

  verifyVoucher(voucherCode){
    this.commonService.verifyVoucher(voucherCode).subscribe(success =>{
      console.log("success",success);
      if(success.status.desc=="Success"){
        this.redeemFlag=false;
        this.redeemData=true;
        let result=success.result;
        this.setToken('selectedRedeemData',JSON.stringify(result));
        this.setToken('voucherCode',voucherCode);
        this.removeToken('successRedeemData');
        this.user.selectedCountryValue=this.countriesData.find(o=>o.alpha2Code==result.destinationCountry.countryCode);
        this.getMsisdn(this.user.selectedCountryValue);
      }
      else{
        this.commonService.popToast('error','Error',success.result.display);
      }
    },error=>{
      this.commonService.popToast('error','Error',error.message);
    });
  }


  allowNumbers(e){
    var input;
    console.log("input==",input,e);
    if (e.metaKey || e.ctrlKey) {
      return true;
    }
    if (e.which === 32) {
      return false;
    }        
    if (e.which === 0) {
      return true;
    }
    if (e.which < 33) {
      return true;
    }
    if (e.which === 43 || e.which === 45|| e.which === 46) {
      return true;
    }
    if (e.which === 189) {
      return true;
    }
    input = e.key;
    console.log("input=",input);
    return !!/[\d\s]/.test(input);
  }


}
