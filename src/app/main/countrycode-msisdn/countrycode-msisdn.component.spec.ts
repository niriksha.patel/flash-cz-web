import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountrycodeMsisdnComponent } from './countrycode-msisdn.component';

describe('CountrycodeMsisdnComponent', () => {
  let component: CountrycodeMsisdnComponent;
  let fixture: ComponentFixture<CountrycodeMsisdnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountrycodeMsisdnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountrycodeMsisdnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
