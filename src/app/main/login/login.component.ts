import { Component, OnInit,ViewEncapsulation,Injector } from '@angular/core';
import { BaseComponent } from '../../common/commonComponent';
import {Location} from '@angular/common';
import { 
  AuthService, 
  FacebookLoginProvider, 
  GoogleLoginProvider,
  LinkedinLoginProvider
} from 'ng4-social-login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends BaseComponent implements OnInit {

  constructor(injector: Injector,public authService: AuthService,public location: Location) {
  	super(injector);
  }

  ngOnInit() {
    this.authService.signOut();
  	 // this.authService.authState.subscribe((user) => {
    //  console.log(user,"==user logged");
    //    if(user)
    //    {
    //      	if(this.getToken('paymentPage'))
    //      	{
    //      		this.setToken("loggedIn",true);
    //      		// this.router.navigate(['/main/order-page']);     		
    //      	}     	
    //    }
    // });
  }


  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then((success)=>{
      console.log("success",success);
      if(success)
      {
        if(this.getToken('paymentPage'))
        {
          this.setToken("loggedIn",true);
          this.router.navigate(['/cz/myflash/order-page']);         
        } 
        else if(this.getToken('redeemPage')){
          this.setToken("loggedIn",true);
             // this.callForRedeemVoucher();
           }      
         }

       });
  }

// callForRedeemVoucher(){
//   this.commonService.redeemVoucher().subscribe(success=>{
//      console.log("success",success);
//      if(success.status.desc=="Success")
//      {
//         this.setToken("loggedIn",true);
//         this.setToken("doneMsg",success.result.display);
//          this.location.back() ;
//      }
//     },error=>{
//        this.commonService.popToast('error','Error',error.message);
//     });
// }


signInWithFB(): void {
  this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
}

signInWithLinkedIN(): void {
  this.authService.signIn(LinkedinLoginProvider.PROVIDER_ID);
}

signOut(): void {
  console.log("signout");
  this.authService.signOut();
}

signup(){
  console.log("called",this.location );

  this.setToken("loggedIn",true);
  if(this.getToken('paymentPage'))
  {
    this.setToken("loggedIn",true);
    this.router.navigate(['/cz/myflash/order-page']);                 
  } 
  else if(this.getToken('redeemPage')){             
             // this.callForRedeemVoucher();
           }  
           else{
             this.location.back() ;  
           }

     // this.router.events.subscribe((e)=>{
     //   console.log("events",e);
     // })

   }


 }
