import { Component, OnInit } from '@angular/core';
import { trigger , state , style , animate , transition, keyframes } from '@angular/animations';
import { Injector, NgZone } from '@angular/core'; 
import { BaseComponent } from '../../common/commonComponent';

declare var WOW : any;


@Component({
	selector: 'app-main',
	templateUrl: './main.component.html'
})
export class MainComponent extends BaseComponent implements OnInit {
	public loginFlag:boolean=false;
	public currentYear:any=new Date().getFullYear();
	public close:boolean=true;
	constructor(injector: Injector) {
		super(injector);
		if(this.cookie.get('acceptCookie')){
			this.close=false;     
		}
		else if(!this.cookie.get('acceptCookie')){
  	// setTimeout(()=>{
  		// this.$('.header-search.cookies').css('display','block');
  		this.close=true;
  	// },500);
  }
  console.log("this.cookie==",this.cookie);
}


ngOnInit() {
		// this.loadScript();
    //this.popToast('success','Welcome','Successfully logged in.')
}
//===================Sticky Header on Scroll
myScrollHandler = (): void => {
	var scroll = this.$(window).scrollTop();
	if (scroll >= 50) {
		this.$('.navbar').addClass('fixed');
	} else {
		this.$('.navbar').removeClass('fixed');
	}
};

ngAfterIntit(){
	new WOW().init();

}

signup(){
	
}

getLoginStatus(){
	if(this.getToken("loggedIn"))
		{return true;}
	else
		{return false;}
}

getCartCount(){

	let cartData=JSON.parse(this.getToken("cartData"));
	if(cartData)
	{
		return cartData.length;
	}     
	else{
		return 0;
	}
}

logout(){
	this.clearToken();
	console.log(this.location.isCurrentPathEqualTo('/main/home'));
	this.router.navigate(['/']);
	if(this.location.isCurrentPathEqualTo('/main/home')){
		location.reload();		
	}
	
}

acceptCookie(){
	let cookiedate = new Date();
	let difference = cookiedate.setMonth(cookiedate.getMonth() + 13)
	document.cookie = 'acceptCookie' + "=" + true + ";expires=" + new Date(difference) + ";path=/";
	this.close=false;
}


}
