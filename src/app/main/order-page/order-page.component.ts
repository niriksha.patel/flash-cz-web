import { Component, OnInit,ViewChild,ElementRef,ViewEncapsulation } from '@angular/core';
import { trigger , state , style , animate , transition, keyframes } from '@angular/animations';
import { Injector, NgZone } from '@angular/core'; 
import { BaseComponent } from '../../common/commonComponent';
import * as $ from 'jquery';
declare var jQuery:any;
declare var $ :any;
declare const fbq: any;
/* ========= Using Thirdparty module CHECKOUT.COM ======== */
declare const cart_Id : any;
// declare function callBackData() : any;
const checkURL = 'https://cdn.checkout.com/v2/sandbox/js/checkout.js';
const urlContents = `window.CKOConfig = {
  publicKey: 'pk_test_80b5d649-7ade-462f-a0e9-b00b07ffa15c',
  customerEmail: 'graydon.hewson@flash.co.za',
  value: 100,
  currency: 'GBP',
  appMode: 'lightbox' ,
  cardTokenised: function(event) {
    cart_Id = event.data;
    document.getElementById('finalTrigger').click();
  }
};`;
/* ========= Using Thirdparty module ======== */



@Component({
	selector: 'app-order-page',
	templateUrl: './order-page.component.html',
	styleUrls: ['./order-page.component.css'],
	encapsulation:ViewEncapsulation.None
})
export class OrderPageComponent extends BaseComponent implements OnInit {
	@ViewChild('tabContent')tabContent:ElementRef
	@ViewChild('step1')step1:ElementRef
  @ViewChild('step2')step2:ElementRef
  @ViewChild('editor')editor:ElementRef
  public editorValue: string = '';
	// @ViewChild('tabContent')tabContent:ElementRef
	// @ViewChild('tabContent')tabContent:ElementRef
	// @ViewChild('tabContent')tabContent:ElementRef
	// @ViewChild('tabContent')tabContent:ElementRef
	// @ViewChild('tabContent')tabContent:ElementRef
  public msisdndata;
  public selectedCountryValue;
  public operatorData;
  public selectedBox;
  public selectedAmount;
  public productsData;
  public selectedOperator:any={};
  public selectedProduct:any={};
  public selectedRedeem;
  public cartData:any=[];	
  public activeAmount:boolean=false;
  public activeButton:boolean=true;
  public countryCode:any;
  public doneDisplayMsg:any;
  public purchasedData:any;
  public txId:any;
  public tabTitle:any="Your Order";

  public tab1:boolean=true;
  public tab2:boolean=false;
  public tab3:boolean=false;
  public tab4:boolean=false;
  public tab5:boolean=false;
  public loading:boolean=false;

  paymentFlag:any=false;
  loadAPI: Promise<any>;
  sessionToken: string;


  constructor(injector: Injector) {
    super(injector);	
    this.operatorData=JSON.parse(this.localStorage.getItem("operatorData"));
    if(this.getToken("cartData")){
      this.cartData=JSON.parse(this.getToken("cartData"));
    }
    console.log("browser name=",navigator);
    console.log("this.cartData",this.cartData);
    // fbq('track', 'Order View');
  }

  ngOnInit() {
    this.loadTawkScript();
    this.msisdndata= this.localStorage.getItem('msisdnData');
    this.countryCode=JSON.parse(this.localStorage.getItem('msisdnCountry'));
    console.log("this.countryCode",this.countryCode);
    this.selectedRedeem=JSON.parse(this.getToken('selectedRedeemData'));
    console.log("this.selectedOperator",this.selectedRedeem);
    if(this.selectedRedeem)
    {
      this.selectedOperator.logoURL=this.selectedRedeem.productProvider.logoURL;
      this.selectedOperator.description=this.selectedRedeem.productProvider.description;
      this.selectedProduct.display=this.selectedRedeem.product.display;
      this.selectedProduct.productID=this.selectedRedeem.product.productID;
      this.selectedProduct.localValue=this.selectedRedeem.product.costLocal.localFaceValue;
      this.selectedProduct.localCurrencyCode=this.selectedRedeem.product.costLocal.localCurrencyCode;
      this.cartData=[{
        "receipent":this.countryCode.callingCodes[0]+this.msisdndata,
        "logoUrl":this.selectedOperator.logoURL,
        "operator":this.selectedOperator.description,
        "amount":this.selectedProduct.display,
        "localValue":this.selectedProduct.localValue,
        "localCurrencyCode": this.selectedProduct.localCurrencyCode,
        "productId":this.selectedProduct.productID
      }]
      this.setToken("cartData",JSON.stringify(this.cartData));
          // this.savedCardData=JSON.parse(this.getToken("cartData"));
          this.myScrollHandler();
          this.tab1=false;
          this.tab2=true;
          this.tab3=false;
          this.tab4=false;
          this.tab5=false;
        }
        if(this.getToken("successRedeemData")){
          this.tab1=false;
          this.tab2=false;
          this.tab3=true;
          this.doneDisplayMsg=this.getToken("successRedeemData");
        }
		// this.selectedCountryValue=this.msisdndata;
		// console.log(this.selectedCountryValue,"this.selectedCountryValue");
		
	}
	ngAfterViewInit(){
    console.log(this.tabContent.nativeElement,"dsfds");
    setTimeout(()=>{
      $('body').removeClass('terms');
    },100)
   // if(this.getToken("loggedIn"))
   // {
     if(this.getToken('redeemPage')) 
     {
       this.doneDisplayMsg=this.getToken("doneMsg");
       this.myScrollHandler();
       this.tab1=false;
       this.tab2=false;
       this.tab3=true;
       // this.tab4=true;
       this.tab5=false;
       console.log("this.doneDisplayMsg",this.doneDisplayMsg);

     } 
     else if(this.getToken('paymentPage')){
       this.tabTitle="Payment";
       this.myScrollHandler();
       this.tab1=false;
       this.tab2=false;
       this.tab3=true;
       this.tab4=false;
       this.tab5=false;   
       this.sendCall() ; 
     }
     //   else{
     //     this.tabTitle="Payment";
     //   this.myScrollHandler();
     //    this.tab1=false;
     //   this.tab2=false;
     //   this.tab3=false;
     //   this.tab4=true;
     //   this.tab5=false;   
     //   this.sendCall() ; 
     // }
     
   // }



 }

	  // Css property for selecting boxes
    getBoxClicked(i,data,type){
      if(type=='operator'){
        this.selectedBox=i;
        this.selectedOperator=data;
        this.selectedAmount=undefined;
        this.productsData=[];
        this.getProviderOperatorData(data);
        this.activeAmount=true;
      // selectedAmount=undefined;
      // this.selectNetwork(data);
    }
    else if(type=='products'){
    	this.selectedProduct=data;
      this.selectedAmount=i;
      this.activeButton=false;
      // this.selectAmount(data);
    }
    
  }

  getProviderOperatorData(data){     	
     	// let countryCode=JSON.parse(this.localStorage.getItem('msisdnCountry'));
       this.loading=true;
       this.commonService.getProducts(data.productProviderID,this.countryCode.alpha2Code).subscribe(success=>{
         this.loading=false;
         if(success.result)
         {
           this.productsData=success.result.products;
         }
         
       },error=>{
         this.commonService.popToast('error','Error',error.message);
       });
     }

     saveToCart(){
       this.selectedBox=undefined;
       this.activeAmount=false;
       this.activeButton=true;
       this.cartData.push({
         "receipent":this.countryCode.callingCodes[0]+this.msisdndata,
         "logoUrl":this.selectedOperator.logoURL,
         "operator":this.selectedOperator.description,
         "amount":this.selectedProduct.display,
         "localValue":this.selectedProduct.costLocal.localFaceValue,
         "localCurrencyCode":this.selectedProduct.costLocal.localCurrencyCode,
         "productId":this.selectedProduct.productID
       });
       this.setToken("cartData",JSON.stringify(this.cartData));
       console.log("this.cartData",this.cartData);
     }
     proceedToCheckout(){
       this.cartData.push({
         "receipent":this.countryCode.callingCodes[0]+this.msisdndata,
         "logoUrl":this.selectedOperator.logoURL,
         "operator":this.selectedOperator.description,
         "amount":this.selectedProduct.display,
         "localValue":this.selectedProduct.costLocal.localFaceValue,
         "localCurrencyCode":this.selectedProduct.costLocal.localCurrencyCode,
         "productId":this.selectedProduct.productID
       });
       this.setToken("cartData",JSON.stringify(this.cartData));
          // this.savedCardData=JSON.parse(this.getToken("cartData"));
          this.myScrollHandler();
          this.tabTitle="Summary";
          this.tab1=false;
          this.tab2=true;
          this.tab3=false;
          this.tab4=false;
          this.tab5=false;
        }

        resetTabs(){
          this.tabTitle="Your Order";
          this.tab1=true;
          this.tab2=false;
          this.tab3=false;
          this.tab4=false;
          this.tab5=false;
          this.activeAmount=false;
          this.selectedBox=undefined;
        }

        removeData(x){
          this.cartData.splice(x,1);
          this.setToken("cartData",JSON.stringify(this.cartData)); 
        }

        myScrollHandler = (): void => {
          var scroll = this.$("html, body").animate({ scrollTop: 0 }, 600);	
        };

        getPrice(price,index){
          if(price){
            let s=price.split(" ");
            return s[(s.length)-index];
          }
        }

        getTotal(){
          let total=0;
          this.cartData.forEach((data)=>{
        // let price=data.localValue;
        total=total+parseInt(data.localValue);
      })
          return total;         
        }


        callForRedeemVoucher(){
          let voucherCode=this.getToken("voucherCode");
          this.commonService.redeemVoucher(this.countryCode.callingCodes[0]+this.msisdndata,voucherCode).subscribe(success=>{
            console.log("success",success);
            
            if(success.status.desc=="Success")
            {
              this.doneDisplayMsg=success.result.display;
              this.myScrollHandler();
              this.tab1=false;
              this.tab2=false;
              this.tab3=true;
              this.setToken("successRedeemData",success.result.display);
              // this.tab4=true;
              // this.tab5=true;
              console.log("this.doneDisplayMsg",this.doneDisplayMsg)
        // this.setToken("doneMsg",success.result.display);
        //  this.location.back() ;
      }
    },error=>{
      this.commonService.popToast('error','Error',error.message);
    });
        }

        goToNextStep(){
          console.log("next step",this.getToken("loggedIn"));
    // if(!this.getToken("loggedIn"))
    // {
    //   this.router.navigate(['/main/login']);      
    //   if(!this.selectedRedeem){
    //     this.setToken('paymentPage',true);
    //   }
    //   else
    //   {
    //      this.setToken('redeemPage',true);
    //   }

    // }
    // else{

      if(!this.selectedRedeem)
      {
        this.tabTitle="Payment";
        this.myScrollHandler();
        this.tab1=false;
        this.tab2=false;
        this.tab3=true;
        this.tab4=false;  
        this.sendCall() ;                 
      } 
      else{             
        this.callForRedeemVoucher();
      }  
       //     else{
       //        this.location.back() ;  
       //     }

       
    // }
    
  }

  formatReceipt(val: string): string {
    var regex = /;/g;
    return this.nl2br(val.replace(regex, "<br/>"), false);
  }

  nl2br(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
  }

  /*===== EVENT ON SEND BUTTON ===*/

  callBackData(){
            // this.presentLoading();
            var urlParams = "cko-public-key=pk_test_80b5d649-7ade-462f-a0e9-b00b07ffa15c&cko-card-token="+cart_Id.cardToken;
            let data={ "transaction": {"cardToken":urlParams}};

            this.commonService.checkout(this.txId,data).subscribe(success=>{
              console.log("suucess",success);
              if(success.status.desc=="Success")
              {

                this.cartData=[];
                // this.purchasedData=success.result.purchaseProducts
                this.purchasedData=success.result;
                this.purchasedData.print=this.formatReceipt(this.purchasedData.print)
                this.myScrollHandler();
                this.tab1=false;
                this.tab2=false;
                this.tab3=false;
                this.tab4=true;
                this.tab5=true;  
              }
            },error=>{
              this.commonService.popToast('error','Error',error.message);
            });

            // this.httpService.checkout(this.sessionToken,urlParams)
            // .then((data)=>{
            //   // this.loader.dismiss();
            //   let resultData=JSON.parse(JSON.stringify(data));
            //   if(resultData.status==500){
            //     // let alert = this.alertCtrl.create({
            //     //   title: '',
            //     //   subTitle: 'Server Error',
            //     //   buttons: [{
            //     //     text: 'Dismiss'
            //     //   }]
            //     // });
            //     // alert.present();
            //   }else{
            //     if(resultData.status.type==0)
            //     {
            //       // let alert = this.alertCtrl.create({
            //       //   title: '',
            //       //   subTitle: 'Transaction Successful',
            //       //   buttons: [{
            //       //     text: 'Okay',
            //       //     handler: () => {
            //       //       console.log('Cancel clicked');
            //       //             //GO BACK TO THE HOME PAGE
            //       this.paymentFlag=false;
            //       //             localStorage.clear();
            //       //             this.editContact();
            //       //           }
            //       //         }],
            //       //         enableBackdropDismiss:false
            //       //       });
            //       // alert.present();
            //     }
            //   }
            // })
          }

          public loadScript(className) {
                  // document.getElementById(className).click()
                  // this.loader.dismiss();
                  let node = document.createElement('script');
                  node.innerHTML = urlContents;
                  node.type = 'text/javascript';
                  node.charset = 'utf-8';
                  console.log(" document.getElementById(className)", document.getElementById(className));
                  document.getElementById(className).appendChild(node);
                  let node1 = document.createElement('script');
                  node1.src = checkURL;
                  node1.type = 'text/javascript';
                  node1.async = true;
                  node1.charset = 'utf-8';
                  document.getElementById(className).appendChild(node1);
                }
// SEND BUTTON CLICK
sendCall(){
  console.log("this.cartData",this.cartData);
  // if(this.selectedContact && this.selectedOperator && this.selectedOperatorPlan)
  // {
  //   let data=JSON.stringify({
  //     "operator": parseInt( this.selectedOperator.id),                    
  //     "msisdn":this.selectedContact.toString(),
  //     "amountOperator":this.selectedOperatorPlan.amountOperator,
  //     "productId": parseInt(this.selectedOperatorPlan.productId)
  //   });
  let purchaseProducts=[];
  for(let i=0;i<this.cartData.length;i++)
  {
    purchaseProducts.push({"index":i,"productID":parseInt(this.cartData[i].productId),"localFaceValue":this.cartData[i].localValue});
  }

  let data={
    "auth":{"key":"sdfsdfsdfdsf34543543"},
    "transaction": {
      "purchaseProducts":purchaseProducts
    },
    "scope": {
      "device": {                            
        "platform":navigator.appName,
        "platformVersion":navigator.appVersion
      },
      "txDateTime":new Date()
    }
  }
  console.log("date",data);
  this.commonService.purchaseProducts(data).subscribe(success=>{
    console.log(success,"success");
    if(success.status.desc=="Success")
    {
      this.txId=success.result.txID;
      this.loadAPI = new Promise((resolve) => {
        this.loadScript('thirdParty');
      });
    }
  },error=>{
    this.commonService.popToast('error','Error',error.message);
  });

                  // let data={
                    //       "operator":148,
                    //       "msisdn":"254703260060",
                    //       "amountOperator":"5",
                    //       "productId":3104
                    //     };
                    // this.presentLoading();
                    // this.httpService.purchase(data).then((data)=>{
                    //   this.paymentFlag=true;
                      // this.ref.detectChanges();
                      // this.presentLoading();
                      
                  //   });
                  // }
                  // else{
                  //   alert("Please fill the required fields first");
                  // }
                }

                done(){
                  this.clearToken();
                  this.router.navigate(['/']);
   // this.authService.signOut();
 }
 

}
