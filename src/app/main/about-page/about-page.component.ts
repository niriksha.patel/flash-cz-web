import { Component, OnInit,OnDestroy,ViewEncapsulation,ViewChild,ElementRef,Inject } from '@angular/core';
import { trigger , state , style , animate , transition, keyframes } from '@angular/animations';
import { Injector, NgZone } from '@angular/core'; 
import { BaseComponent } from '../../common/commonComponent';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { PageScrollConfig, PageScrollService, PageScrollInstance } from 'ng2-page-scroll';
import { DOCUMENT} from '@angular/common';
import * as $ from 'jquery';
declare var jQuery:any;
declare var $ :any;
declare const fbq: any;

@Component({
	selector: 'app-about-page',
	templateUrl: './about-page.component.html',
	styleUrls: ['./about-page.component.css'],
	encapsulation:ViewEncapsulation.None
})
export class AboutPageComponent extends BaseComponent implements OnInit {
	@ViewChild('terms') terms : ElementRef;
	constructor(injector: Injector,private pageScrollService: PageScrollService, @Inject(DOCUMENT) private document: any) {
		super(injector);
		// fbq('track', 'About Us View');
		
	}

	ngOnInit() {
		// if(isPlatformBrowser(this.platformId)){
			// $('html, body').animate({ scrollTop:$(".termsPage").offset().top}, 500);
			// setTimeout(()=>{
			// 	$('body').addClass('terms');
			// 	$('iframe').css('display','none')
			// },100)
		// }
	}
	ngAfterViewInit(){
		let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '#terms');
		this.pageScrollService.start(pageScrollInstance);
		$('html, body').animate({ scrollTop:$(".termsPage").offset().top}, 500);
		setTimeout(()=>{
			// $('body').addClass('terms');
			$('iframe').css('display','none')
		},100)
	}
	// ngOnDestroy(){
	// 	setTimeout(()=>{
	// 		$('body').removeClass('terms');
	// 	},100)
	// }

}
