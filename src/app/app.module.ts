import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, PreloadAllModules} from '@angular/router';
import { AppComponent } from './app.component';
import { FormsModule }   from '@angular/forms';
import { CoolStorageModule } from 'angular2-cool-storage';
import {ToasterModule, ToasterService} from 'angular2-toaster';
export { AppComponent };
import { BaseComponent } from './common/commonComponent';
import { AlwaysAuthChildrenGuard, CanLoginActivate } from './common/auth.gaurd';
import { CookieService } from 'ngx-cookie-service';
import { CommonService } from './common/common.service';
import { HttpModule } from '@angular/http';

import { HomePageComponent } from './main/home-page/home-page.component';
import { MainComponent } from './main/main/main.component';
import { AboutPageComponent } from './main/about-page/about-page.component';
import { OrderPageComponent } from './main/order-page/order-page.component';
import { LoginComponent } from './main/login/login.component';
import { TermsComponent } from './main/terms/terms.component';
import {Ng2PageScrollModule} from 'ng2-page-scroll';
import { CountrycodeMsisdnComponent } from './main/countrycode-msisdn/countrycode-msisdn.component';
import { ConfirmDialog } from './confirm-dialog.component';
import { ErrorMessages } from './common/errorMessages';

import { NgProgressModule, NgProgressBrowserXhr } from 'ngx-progressbar';
import { BrowserXhr} from '@angular/http';
import { TextMaskModule } from 'angular2-text-mask';
import {
  SocialLoginModule, 
  AuthServiceConfig,
  GoogleLoginProvider, 
  FacebookLoginProvider, 
  LinkedinLoginProvider
} from 'ng4-social-login';
import { CookiesPrivacyComponent } from './main/cookies-privacy/cookies-privacy.component';
import { FaqComponent } from './main/faq/faq.component';
import { EmailDisclaimerComponent } from './main/email-disclaimer/email-disclaimer.component';

const CONFIG = new AuthServiceConfig([
{
  id: GoogleLoginProvider.PROVIDER_ID,
  provider: new GoogleLoginProvider('62707754693-96oug9n9qv46etu4gs2klq5dbeuil3hp.apps.googleusercontent.com')
    // provider: new GoogleLoginProvider('62707754693-96oug9n9qv46etu4gs2klq5dbeuil3hp.apps.googleusercontent.com')
  }
  // {
  //   id: FacebookLoginProvider.PROVIDER_ID,
  //   provider: new FacebookLoginProvider('Facebook-App-Id')
  // },
  // {
  //   id: LinkedinLoginProvider.PROVIDER_ID,
  //   provider: new LinkedinLoginProvider('LINKEDIN_CLIENT_ID')
  // }
  ]);

export function provideConfig() {
  return CONFIG;
}


@NgModule({
  declarations: [
  AppComponent,
  BaseComponent,
  MainComponent,
  HomePageComponent,
  AboutPageComponent,
  OrderPageComponent,
  LoginComponent,
  TermsComponent,
  CountrycodeMsisdnComponent,
  ConfirmDialog,
  CookiesPrivacyComponent,
  FaqComponent,
  EmailDisclaimerComponent
  

  ],
  imports: [
  NgProgressModule,
  TextMaskModule,
  HttpModule,
  Ng2PageScrollModule,
  CoolStorageModule, ToasterModule,
  FormsModule, 
  SocialLoginModule, 
  BrowserModule.withServerTransition({ appId: 'FlashUKWeb'}),
  RouterModule.forRoot([
    { path: 'cz/myflash', component: MainComponent, children : [
    { path: '', redirectTo: 'home', pathMatch: 'full'},
    { path: 'home', component: HomePageComponent, pathMatch: 'full'},
    // { path: 'home/:id1/:id2/:id3', component: HomePageComponent, pathMatch: 'full'},
    { path: 'about', component: AboutPageComponent, pathMatch: 'full'},
    { path: 'login', component: LoginComponent, pathMatch: 'full'},
    { path: 'order-page', component: OrderPageComponent, pathMatch: 'full'},
    // { path: 'account-overview', component: AccountOverviewComponent, pathMatch: 'full'},
    { path: 'terms', component: TermsComponent, pathMatch: 'full'},
    { path: 'cookies-privacy', component: CookiesPrivacyComponent, pathMatch: 'full'},
    { path: 'disclaimer', component: EmailDisclaimerComponent, pathMatch: 'full'},
    { path: 'faq', component: FaqComponent, pathMatch: 'full'},
    { path: '**', redirectTo: 'home', pathMatch: 'full' }
    ]
  },
  // { path: 'cz/myflash/terms', component: TermsComponent, pathMatch: 'full'},
  { path: '', redirectTo : 'cz/myflash', pathMatch : 'full'},
  { path: '**', redirectTo: '', pathMatch: 'full' }

  ], {initialNavigation:'enabled', useHash: false})
  ],
  exports: [AppComponent],
  providers: [AlwaysAuthChildrenGuard,CanLoginActivate, CookieService, CommonService, ToasterService,{
    provide: AuthServiceConfig,
    useFactory: provideConfig
  },{ provide: BrowserXhr, useClass: NgProgressBrowserXhr },
  ErrorMessages],
  bootstrap: [AppComponent]
})
export class AppModule { }