export const environment = {
	env: 'prod',
	production: true,
  apiUrl : 'https://api.theflashcow.com/api/web/v1/', //Please use live (eg. aws) url here. after deployment.
  // apiUrl : 'http://35.205.55.207/api/web/v1/', //Please use live (eg. aws) url here. after deployment.
  port : 8080
};
