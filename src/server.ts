import 'reflect-metadata';
import 'zone.js/dist/zone-node';
import { renderModuleFactory } from '@angular/platform-server'
import { enableProdMode } from '@angular/core'
import { AppServerModuleNgFactory } from '../dist/ngfactory/src/app/app.server.module.ngfactory'
import * as express from 'express';
import { readFileSync } from 'fs';
import { join } from 'path';

// let portEnv = process.env.npm_config_env
// var env;

// switch(portEnv){
// 	case 'prod' : 
// 		env = require('./environments/environment.prod');
// 	break;
// 	case 'dev' : 
// 		env = require('./environments/environment.dev');
// 	break;
// 	case 'staging' : 
// 		env = require('./environments/environment.staging');
// 	break;
// 	default: 
// 		env = require('./environments/environment.dev');
// 	break;
// }

const PORT = process.env.PORT || 8080;

enableProdMode();

const app = express();

let template = readFileSync(join(__dirname, '..', 'dist', 'index.html')).toString();

app.engine('html', (_, options, callback) => {
  const opts = { document: template, url: options.req.url };

  renderModuleFactory(AppServerModuleNgFactory, opts)
    .then(html => callback(null, html));
});

app.set('view engine', 'html');
app.set('views', 'dist')

app.get('*.*', express.static(join(__dirname, '..', 'dist')));

app.get('*', (req, res) => {
  res.render('index', { req });
});

app.listen(PORT, () => {
  console.log(`listening on http://localhost:${PORT}!`);
});
