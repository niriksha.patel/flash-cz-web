const path = require('path');
const nodeExternals = require('webpack-node-externals');
const webpack = require('webpack');

module.exports = {
  entry: {
    server: './src/server.ts'
  },
  resolve: {
    extensions: ['.ts', '.js']
  },
  target: 'node',
  externals: [nodeExternals({
   whitelist: [
   /^@angular\/material/,
   /^@ng-bootstrap\/ng-bootstrap/,
   /^angular2-toaster/,
   /^ngx-cookie-service/,
   /^ng2-page-scroll/,
   /^ng2-ckeditor/
   ]
 })],
  node: {
    __dirname: true
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: '[name].js'
  },
  module: {
    rules: [
    { test: /\.ts$/, loader: 'ts-loader' }
    ]
  }
  // ,plugins: [
  // new webpack.DefinePlugin({
  //   window: undefined,
  //   document: undefined,
        // location: JSON.stringify({
        //     protocol: 'https',
        //     host: `localhost`,
        // })
      // })
    // new webpack.DefinePlugin({
    //   'process.env': {
    //     NODE_ENV: JSON.stringify(process.env.NODE_ENV),
    //     API_URL: JSON.stringify(process.env.API_URL),
    //   },
    // }),
    // ]
  }
